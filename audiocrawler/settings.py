# Scrapy settings for crawler project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'audiocrawler'
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'

SPIDER_MODULES = ['audiocrawler.spiders']
NEWSPIDER_MODULE = 'audiocrawler.spiders'

FEED_FORMAT = 'json'
FILES_URLS_FIELD = 'url'

MP3DOWNLOADER_URL = 'http://inspector.dev/task'
MP3DOWNLOADER_SAVEPATH = '/forge/inspector/storage/files/'
