""" Base spider for MP3 data crawler
"""
import re
import json

from urlparse import urljoin
from datetime import datetime
from random import randint

from scrapy import log
from scrapy.conf import settings

from scrapy.spider import Spider
from scrapy.http import Request

from audiocrawler.utils import *
from audiocrawler.items import MP3Item


class MP3Spider(Spider):
    name = ''

    json_path = ''
    results = 3
    queue_num = 1
    will_download = False
    url = ''

    queue_slots = {}

    match_threshold = 80.0
    max_bitrate = 320.0
    bitrate_weight = 10


    def __init__(self, *args, **kwargs):
        super(MP3Spider, self).__init__(*args, **kwargs)
        settings.set('CONCURRENT_ITEMS', 1)
        settings.set('CONCURRENT_REQUESTS', 1)

        if 'dl' in kwargs:
            self.will_download = True
            settings.set('FILES_STORE', 'downloads/')
            settings.set('ITEM_PIPELINES', {
                'audiocrawler.pipelines.FileURLAppendPipeline': 100,
                'audiocrawler.pipelines.MP3DownloadPipeline': 300,
                'audiocrawler.pipelines.MP3TaggerPipeline': 400
            })
        else:
            settings.set(
                'FEED_URI',
                "json/%s-%s.json" % (self.name, str(datetime.now()))
            )

        if 'p' in kwargs:
            self.json_path = kwargs['p']

        if 'q' in kwargs:
            self.queue_num = kwargs['q']


    def start_requests(self):
        log.msg("Opening: " + self.json_path, level=log.INFO)
        fileobj = open(self.json_path, 'rb')
        rows = json.loads(fileobj.read())

        for row in rows:
            if str(row['queue']) != self.queue_num:
                log.msg('SKIPPING %s - %s' % (row['artist'], row['recording']))
                continue
            request = Request(urljoin(self.url, self.get_search_term(row).strip()))
            request.meta['song_data'] = row
            request.meta['download_slot'] = row['artist'] + row['recording']
            yield request


    def parse(self, response):
        return self.parse_best_match({})


    def parse_best_match(self, best_match):
        try:
            for song in best_match:
                item = MP3Item()

                for key, value in song.items():
                    item[key] = value

                if item['realfilename'] not in self.queue_slots:
                    self.queue_slots[item['realfilename']] = randint(1, 3)

                item['spidername'] = self.name
                # item['queue'] = self.queue_slots[item['realfilename']]
                item['realfilename'] = item['realfilename'].replace('/', '-')

                if int(self.queue_num) == item['queue'] or self.will_download == False:
                    yield item
        except:
            log.msg("MP3Item does not exist", level=log.WARNING)


    def get_search_term(self, row):
        pass


    def best_match(self, songs):
        songs = sorted(songs, key=lambda i: i['match'])
        best_100 = []

        for song in songs:
            if song['match'] == 100.0:
                best_100.append(song)

        if len(best_100) >= self.results:
            for i, song in enumerate(best_100[:]):
                best_100[i]['match'] = self.calc_weight(song['bitrate'], song['match'])

            return sorted(best_100, key=lambda i: i['match'], reverse=True)[:self.results]

        else:
            for i, song in enumerate(songs[:]):
                songs[i]['match'] = self.calc_weight(song['bitrate'], song['match'])

            return sorted(songs, key=lambda i: i['match'], reverse=True)[:self.results]


    def calc_match(self, song):
        omits = ['mp3', 'remix', ' and ']
        str1 = normalize_string(song['realfilename'], omits)
        str2 = normalize_string(song['filename'], omits)
        lcs = longest_common_substring(str1, str2)

        try:
            match1 = (len(lcs) * 1.0) / len(str1) * 100.0
        except:
            match1 = 0
        try:
            match2 = (len(lcs) * 1.0) / len(str2) * 100.0
        except:
            match2 = 0

        return (match1 + match2) / 2


    def calc_weight(self, bitrate, name_match):
        bitrate = int(bitrate.replace('kbps', '').strip()) if len(bitrate) else 0

        return int(bitrate) / self.max_bitrate * 100 * (self.bitrate_weight / 100.0) +\
               name_match * (100 - self.bitrate_weight) / 100.0
