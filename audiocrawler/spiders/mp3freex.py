""" scrapy crawl mp3skull -a p="json/preparer-2014-10-12.json"
"""
import re
import copy

from scrapy.selector import Selector

from audiocrawler.utils import find_value
from audiocrawler.base.mp3spider import MP3Spider


class MP3Freex(MP3Spider):
    name = 'mp3freex'
    url = 'http://mp3freex.com/'

    def get_search_term(self, row):
        artist = row['artist'].encode('ascii', 'replace')
        recording = row['recording'].encode('ascii', 'replace')
        term = re.sub(r'[^\w.]', '-', "-".join([artist, recording])) + '-download'
        return re.sub(r'-{2,}', '-', term).lower()

    def parse(self, response):
        sel = Selector(response)
        songs = []

        for mp3 in sel.xpath("//div[@class='actl']"):
            song = response.meta['song_data']
            song['match'] = -1
            details = ''.join(mp3.xpath(".//span[@class='label label-info']/text()").extract())

            song['realfilename'] = " - ".join([song['artist'], song['recording']])
            song['filename'] = ''.join(mp3.xpath(".//span[@class='res_title']/text()").extract()).strip()
            song['url'] = mp3.xpath(".//a[contains(@class, 'mp3download')]/@href").extract()
            song['time'] = details.split('|')[0].strip()
            song['size'] = ''
            song['bitrate'] = find_value(details, 'kbps')

            urls = []
            for url in copy.copy(song['url']):
                urls.append(url.replace('/download-mp3/', '/mp3file/') + '.mp3')

            song['url'] = urls

            if song['filename']:
                song['match'] = self.calc_match(song)

            songs.append(copy.copy(song))

        return self.parse_best_match(self.best_match(songs))
