""" scrapy crawl merged -a p="json/preparer-2015-06-26.json" -a dl=1 -a q=1
"""

import urllib
import copy

from importlib import import_module
from hashlib import md5

from scrapy.http import Request
from scrapy.selector import Selector

from audiocrawler.utils import find_value
from audiocrawler.base.mp3spider import MP3Spider

class Merged(MP3Spider):
    name = 'merged'
    download_delay = 3
    mp3spiders = [
        # 'MP3Chief',
        # 'MP3Skull',
        'MP3Freex',
        'MP3GoEar',
    ]

    spider_args = []
    spider_kwargs = {}
    spider_items = {}
    spider_items_len = 0


    def __init__(self, *args, **kwargs):
        super(Merged, self).__init__(*args, **kwargs)
        self.spider_args = args
        self.spider_kwargs = kwargs

    def start_requests(self):
        for elem, mp3spider in enumerate(self.mp3spiders):
            module = import_module('.' + mp3spider.lower(), 'audiocrawler.spiders')
            spider_cls = getattr(module, mp3spider)(self.spider_args, **self.spider_kwargs)
            for request in spider_cls.start_requests():
                self.spider_items_len += 1
                request.meta['object'] = spider_cls
                request.meta['callback'] = 'parse'
                request.meta['elem'] = elem
                yield request

    def parse(self, response):
        self.spider_items_len -= 1
        spider_cls = response.meta['object']
        callback = response.meta['callback']
        for item in getattr(spider_cls, callback)(response):
            item['spidername'] = spider_cls.__class__.__name__.lower()
            self.spider_items[md5(item['url'][0].encode('ascii', 'replace'))] = item
        if self.spider_items_len < 1:
            sorted_items = sorted(
                self.spider_items.values(),
                key=lambda item: (item['realfilename'], item['match'], item['size']),
                reverse=True
            )
            for item in sorted_items:
                yield item





