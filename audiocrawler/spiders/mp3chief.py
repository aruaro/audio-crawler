""" scrapy crawl mp3chief -a p="json/preparer-2014-10-12.json"
"""

import urllib
import copy

from scrapy.selector import Selector

from audiocrawler.utils import find_value
from audiocrawler.base.mp3spider import MP3Spider


class MP3Chief(MP3Spider):
    name = 'mp3chief'
    url = 'http://www.mp3chief.com/search'

    def get_search_term(self, row):
        artist = row['artist'].encode('ascii', 'replace')
        recording = row['recording'].encode('ascii', 'replace')
        return "?" + urllib.urlencode({"q": " ".join([artist, recording])})

    def parse(self, response):
        sel = Selector(response)
        songs = []

        for mp3 in sel.css("ul#songs-list > li"):
            song = response.meta['song_data']
            song['match'] = -1
            details = mp3.css("div.song-secondary-col::text").extract()

            song['realfilename'] = " - ".join([song['artist'], song['recording']])
            song['filename'] = ''.join(
                mp3.css("div.song-primary-col > span *::text").extract())
            song['url'] = mp3.css("*::attr(data-url)").extract()
            song['time'] = find_value(details, ':')
            song['size'] = find_value(details, 'MB')
            song['bitrate'] = find_value(details, 'kbps')

            if song['filename']:
                song['match'] = self.calc_match(song)
            else:
                song['match'] = 0

            songs.append(copy.copy(song))

        return self.parse_best_match(self.best_match(songs))
