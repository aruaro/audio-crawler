""" scrapy crawl mp3skull -a p="json/preparer-2014-10-12.json"
"""

import re
import copy

from scrapy.selector import Selector

from audiocrawler.utils import find_value
from audiocrawler.base.mp3spider import MP3Spider


class MP3Skull(MP3Spider):
    name = 'mp3skull'
    url = 'http://mp3skull.cr/mp3/'

    def get_search_term(self, row):
        artist = row['artist'].encode('ascii', 'replace')
        recording = row['recording'].encode('ascii', 'replace')
        term = "_".join([artist, recording]).lower() + ".html"
        term = re.sub(r'[^\w.]', '_', term)
        return re.sub(r'_{2,}', '_', term)

    def parse(self, response):
        sel = Selector(response)
        songs = []

        for mp3 in sel.css("div#song_html"):
            song = response.meta['song_data']
            song['match'] = -1
            details = mp3.css("div.left::text").extract()

            song['realfilename'] = " - ".join([song['artist'], song['recording']])
            song['filename'] = ''.join(mp3.css("div.mp3_title b::text").extract()).strip()
            song['url'] = mp3.css("div.download_button a::attr(href)").extract()
            song['time'] = find_value(details, ':')
            song['size'] = find_value(details, 'mb')
            song['bitrate'] = find_value(details, 'kbps')

            if song['filename']:
                song['match'] = self.calc_match(song)

            songs.append(copy.copy(song))

        return self.parse_best_match(self.best_match(songs))
