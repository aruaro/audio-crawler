""" scrapy crawl preparer -a p="songlist/spotify.html"
"""
import os

from datetime import date

from scrapy.conf import settings
from scrapy.selector import Selector
from scrapy.spider import Spider

from audiocrawler.items import MP3Item


class Preparer(Spider):

    name = 'preparer'
    download_delay = 2


    def __init__(self, *args, **kwargs):
        super(Preparer, self).__init__(*args, **kwargs)
        settings.overrides['CONCURRENT_REQUESTS'] = 1
        settings.overrides['FEED_URI'] = "json/%s-%s.json" %\
            (self.name, str(date.today()))
        if kwargs['p']:
            self.start_urls = ["file://%s/../../%s" %
                               (os.path.dirname(__file__), kwargs['p'])]

    def parse(self, response):
        sel = Selector(response)

        # Pandora
        for key, mp3 in enumerate(sel.css("div.trackData")):
            item = MP3Item(spidername=self.name)
            item['queue'] = int(key) % 3 + 1
            item['recording'] = mp3.css("a.songTitle::text").extract()[0].strip()
            item['artist'] = mp3.css(
                "a.artistSummary::text").extract()[0].strip()
            album = mp3.css("a.albumTitle::text")

            if album:
                item['release'] = album.extract()[0].strip()
            # song['year']   = mp3.css("a.songTitle::text").extract()[0]

            yield item

        # 8Tracks
        for key, mp3 in enumerate(sel.css("ul#tracks_played > li")):
            item = MP3Item(spidername=self.name)
            item['queue'] = int(key) % 3 + 1
            item['recording'] = mp3.css(
                "div.title_artist span.t::text").extract()[0].strip()
            item['artist'] = mp3.css(
                "div.title_artist span.a::text").extract()[0].strip()
            album = mp3.css("div.track_metadata div.album span.detail::text")
            year = mp3.css("div.track_metadata div.year span.detail::text")

            if album:
                item['release'] = album.extract()[0].strip()
            if year:
                item['date'] = year.extract()[0].strip()

            yield item

        # Jango
        for key, mp3 in enumerate(sel.css("div#player_info")):
            item = MP3Item(spidername=self.name)
            item['queue'] = int(key) % 3 + 1
            item['recording'] = mp3.css(
                "span#current-song::text").extract()[0].strip()
            item['artist'] = mp3.css(
                "div#player_current_artist a::text").extract()[0].strip()

            yield item

        # Spotify
        for key, mp3 in enumerate(sel.xpath("//tr[contains(@data-uri, 'spotify')]")):
            item = MP3Item(spidername=self.name)
            item['queue'] = int(key) % 3 + 1
            item['recording'] = mp3.xpath("td[3]/div/text()").extract()[0].strip()
            item['artist'] = mp3.xpath("td[4]/a/text()").extract()[0].strip()
            item['release'] = mp3.xpath("td[5]/a/text()").extract()[0].strip()
            item['time'] = mp3.xpath("td[7]/text()").extract()[0].strip()

            yield item
