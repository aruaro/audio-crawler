""" Mutagen tagger class"""
import re
import json
import musicbrainzngs

from collections import defaultdict

from scrapy import log
from mutagen.easyid3 import EasyID3


class MP3Tagger(object):
    """docstring for ClassName"""


    def do_lookup(self, song_data, file_path):
        """do_lookup"""
        query = []
        song_params = {
            'recording': '',
            'release': '',
            'artist': '',
            'primarytype': 'album',
            'status': 'official'
        }

        for key, value in song_params.items():
            if not value and key in song_data:
                value = re.sub(r'[^\w ]', ' ', song_data[key].lower())
                value = '"'+re.sub(r'(\s+)', ' ', value.strip())+'"'

            query.append('%s:%s' % (key, value))

        time = ''

        if song_data['time']:
            song_data['time'] = song_data['time'].split(':')
            time = int(song_data['time'][0]) * 1000 * 60 + int(song_data['time'][1]) * 1000

        import logging
        logging.basicConfig(level=logging.DEBUG)

        musicbrainzngs.set_useragent("ocd-mp3-tagger", "0.1")

        log.msg('MUSICBRAINZ RECORDING: %s' % ' AND '.join(query))

        results = musicbrainzngs.search_recordings(
            recording=song_data['recording'],
            release=song_data['release'],
            artist=song_data['artist'],
            primarytype='album',
            status='official',
            strict=True,
            limit=1
        )

        for recording in results['recording-list']:
            audio = EasyID3(file_path)
            # for key, value in self.do_lookup_release(recording).items():
            for key, value in song_data.items():
                if value:
                    log.msg('MUSICBRAINZ RESULT: %s - %s' % (key, value))
                    audio[key] = unicode(value)
            audio.save()


    def do_lookup_release(self, recording):
        """do_lookup_release"""
        results = musicbrainzngs.browse_releases(
            recording=recording['id'],
            release_status=['official'],
            release_type=['album'],
            includes=['labels', 'media', 'release-groups'],
            limit=1
        )

        for release in results['release-list']:
            return self.map_results_to_tags(recording, release)


    def map_results_to_tags(self, _recording, _release):
        """map_results_to_tags"""
        recording = defaultdict(str)
        recording.update(_recording)

        release = defaultdict(str)
        release.update(_release)

        release2 = self.get_release_from_recording(release['id'], recording)

        return {
            'acoustid_fingerprint': '',
            'acoustid_id': '',
            'album': self.get_path(release, 'title'),
            'albumartistsort': self.get_path(recording, 'artist-credit/artist/sort-name'),
            'albumsort': self.get_path(release, 'title'),
            'arranger': '',
            'artist': self.get_path(recording, 'artist-credit/artist/name'),
            'artistsort': self.get_path(recording, 'artist-credit/artist/sort-name'),
            'asin': self.get_path(release, 'asin'),
            'author': '',
            'barcode': self.get_path(release, 'barcode'),
            'bpm': '',
            'catalognumber': self.get_path(release, 'label-info-list/catalog-number'),
            'compilation': '',
            'composer': '',
            'composersort': '',
            'conductor': '',
            'copyright': '',
            'date': self.get_path(release, 'date'),
            'discnumber': self.get_path(release, 'medium-list/position'),
            'discsubtitle': '',
            'encodedby': '',
            'genre': self.get_genre(recording['tag-list']),
            'isrc': ''.join(filter(None, recording['isrc-list'])[:1]),
            'language': self.get_path(release, 'text-representation/language'),
            'length': self.get_path(recording, 'length'),
            'lyricist': '',
            'media': '',
            'mood': '',
            'musicbrainz_albumartistid': '',
            'musicbrainz_albumid': self.get_path(release, 'id'),
            'musicbrainz_albumstatus': self.get_path(release, 'status'),
            'musicbrainz_albumtype': self.get_path(release, 'release-group/type'),
            'musicbrainz_artistid': self.get_path(recording, 'artist-credit/artist/id'),
            'musicbrainz_discid': '',
            'musicbrainz_releasegroupid': self.get_path(release, 'release-group/id'),
            'musicbrainz_releasetrackid': self.get_path(release2, 'medium-list/track-list/id'),
            'musicbrainz_trackid': self.get_path(release2, 'medium-list/track-list/id'),
            'musicbrainz_trmid': '',
            'musicbrainz_workid': '',
            'musicip_fingerprint': '',
            'musicip_puid': '',
            'organization': '',
            'originaldate': '',
            'performer': '',
            'performer:*': '',
            'releasecountry': self.get_path(release, 'country'),
            'replaygain_*_gain': '',
            'replaygain_*_peak': '',
            'title': self.get_path(recording, 'title'),
            'titlesort': self.get_path(recording, 'title'),
            'tracknumber': self.get_path(release, 'medium-list/track-count'),
            'version': '',
            'website': ''
        }


    def get_path(self, dict_obj, path_name):
        """get_path"""
        for path in path_name.split('/'):
            if type(dict_obj[path]) == list:
                for obj_elm in filter(None, dict_obj[path]):
                    dict_obj = obj_elm
                    break
            else:
                dict_obj = dict_obj[path]

        return dict_obj


    def get_release_from_recording(self, release_id, recording):
        """get_release_from_recording"""
        for release in recording['release-list']:
            if release['id'] == release_id:
                return release


    def get_genre(self, genres):
        """get_genre"""
        if len(genres) >= 1:
            return sorted(
                genres,
                key=lambda tag: int(tag['count'])
            )[0]['name'].title()
