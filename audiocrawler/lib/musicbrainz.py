""" scrapy crawl preparer -a p="/forge/audio-crawler/songlist/sunday.html"
"""
import urllib

from scrapy.selector import Selector
from scrapy.http import Request
from scrapy.spider import Spider

from audiocrawler.items import MP3ItemLoader


class MusicBrainz(Spider):

    name = "musicbrainz"


    def do_lookup(self, song_data):
        url = 'http://musicbrainz.org/ws/2/recording'
        song_params = {
            'recording': '"%s"' % song_data['recording'] if song_data['recording'] else '',
            'release': '"%s"' % song_data['release'] if song_data['release'] else '',
            'artist': '"%s"' % song_data['artist'] if song_data['artist'] else '',
            'primarytype': 'album',
            'status': 'official'
        }
        params = {
            'query': [],
            'limit': 1
        }

        for key, value in song_params.items():
            params['query'].append("%s:%s" % (key, value))

        params['query'] = ' AND '.join(params['query'])

        yield Request(
            "%s?%s" % (url, urllib.urlencode(params)),
            callback=self.parse_lookup
        )


    def parse_lookup(self, response):
        sel = Selector(response)
        sel.remove_namespaces()

        url = 'http://musicbrainz.org/ws/2/release'
        params = {
            'recording': ''.join(sel.xpath('//recording[1]/@id').extract()),
            'status': 'official',
            'type': 'album|single',
            'inc': 'labels+release-groups+media',
            'limit': 1
        }

        yield Request(
            "%s?%s" % (url, urllib.urlencode(params)),
            meta={'recording': sel.xpath('//recording[1]')},
            callback=self.parse_release
        )


    def parse_release(self, response):
        sel = Selector(response)
        sel.remove_namespaces()

        loader = MP3ItemLoader(selector=sel)
        parse_time = lambda timestr: "%2d:%0.2f" % ((int(''.join(timestr))/1000/60), (int(''.join(timestr))%1000%60))

        loader.add_xpath('release', '//release/title/text()')
        loader.add_xpath('release_id', '//release/@id')
        # loader.add_xpath('release_type', '')
        loader.add_xpath('release_format', '//release/medium-list/medium[1]/format/text()')
        loader.add_xpath('release_status', '//release/status/text()')
        loader.add_xpath('release_quality', '//release/quality/text()')
        loader.add_xpath('release_group_id', '//release/release_group/@id')
        loader.add_xpath('release_group_type', '//release/release_group/@type')

        if ''.join(loader.get_xpath('//release/release_group/@type')) == 'Compilation':
            loader.add_value('album_artist', 'Various Artists')
            loader.add_value('is_compilation', 'true')
        else:
            loader.add_value('is_compilation', 'false')


        loader.add_xpath('script_code', '//release/text-representation/script/text()')
        loader.add_xpath('language', '//release/text-representation/language/text()')
        loader.add_xpath('date', '//release/date/text()')
        loader.add_xpath('country', '//release/country/text()')
        loader.add_xpath('barcode', '//barcode/text()')
        loader.add_xpath('asin', '//release/asin/text()')

        loader.add_xpath('label', '//release//label-info[1]/label/name/text()')
        loader.add_xpath('label_id', '//release/label-info[1]/label/@id')
        loader.add_xpath('catalogue_number', '//release/label-info[1]/catalog-number/text()')
        loader.add_xpath('total_mediums', '//release/medium-list/@count')

        recording = response.meta['recording'].xpath(
            '//release[@id="%s"]' % ''.join(loader.get_xpath('//release/@id'))
        )
        loader.selector = recording
        loader.add_xpath('recording', '//recording/title/text()')
        loader.add_xpath('artist', '//recording/artist-credit//artist/name/text()')
        loader.add_xpath('artist_sort', '//recording/artist-credit//artist/sort-name/text()')
        loader.add_xpath('album_artist', '//recording/artist-credit//artist/name/text()')
        loader.add_xpath('artist_id', '//recording/artist-credit//artist/@id')
        loader.add_value('genre', self.get_genre(recording.xpath('//tag')))
        loader.add_xpath('isrc', '//isrc[1]/@id')
        loader.add_xpath('track_id', '//track[1]/@id')
        loader.add_xpath('track_number', '//track[1]/number/text()')
        loader.add_value('time', loader.get_xpath('//track[1]/length/text()'), parse_time)

        return loader.load_item()

    def get_genre(self, tags):
        tags_list = []

        for tag in tags:
            tags_list.append({
                'name': tag.xpath('name/text()').extract()[0],
                'count': tag.xpath('@count').extract()[0]
            })

        sorted(tags_list, key=lambda tag: tag['count'])

        if not tags_list:
            return ''

        return tags_list.pop()['name']
