""" Miscellaneous functions module
"""

import re


def longest_common_substring(a, b):
    m = [[0] * (1 + len(b)) for i in xrange(1 + len(a))]
    longest, x_longest = 0, 0
    for x in xrange(1, 1 + len(a)):
        for y in xrange(1, 1 + len(b)):
            if a[x - 1] == b[y - 1]:
                m[x][y] = m[x - 1][y - 1] + 1
                if m[x][y] > longest:
                    longest = m[x][y]
                    x_longest = x
            else:
                m[x][y] = 0

    return a[x_longest - longest: x_longest]


def levenshtein(a, b):
    "Calculates the Levenshtein distance between a and b."
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a, b = b, a
        n, m = m, n

    current = range(n + 1)
    for i in range(1, m + 1):
        previous, current = current, [i] + [0] * n
        for j in range(1, n + 1):
            add, delete = previous[j] + 1, current[j - 1] + 1
            change = previous[j - 1]
            if a[j - 1] != b[i - 1]:
                change = change + 1
            current[j] = min(add, delete, change)

    return current[n]


def normalize_string(string, omits):
    string = re.sub('[^0-9a-zA-Z]+', ' ', string).lower()
    string = re.sub(r"%s" % '|'.join(omits), '', string, re.IGNORECASE)

    string = string.replace(' ', '')

    return string.strip()


def find_value(values, key):
    for value in values:
        if value.find(key) > -1:
            return value.strip().lower()

    return ''
