# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
#

import os
import re
import urllib
import urlparse

try:
    from cStringIO import StringIO as BytesIO
except ImportError:
    from io import BytesIO

from scrapy import log
from scrapy.conf import settings
from scrapy.http import FormRequest
from scrapy.exceptions import DropItem
from scrapy.utils.misc import md5sum
from scrapy.contrib.pipeline.media import MediaPipeline
from scrapy.contrib.pipeline.files import FilesPipeline

from audiocrawler.lib.mp3tagger import MP3Tagger


ALPHANUM_RE = re.compile(r'[^A-Za-z0-9]+')


class CrawlerPipeline(object):

    def process_item(self, item, spider):
        return item


class FileURLAppendPipeline(object):

    def process_item(self, item, spider):
        url_parts = list(urlparse.urlparse(item['url'][0]))
        query = dict(urlparse.parse_qsl(url_parts[4]))
        query.update({'amifilename123': item['realfilename']})

        url_parts[4] = urllib.urlencode(query)

        item['url'] = [urlparse.urlunparse(url_parts)]

        filepath = "%sfull/%s.mp3" % (settings.get('FILES_STORE'),
                                      item['realfilename'])

        if not os.path.isfile(filepath):
            return item
        else:
            raise DropItem('File exists')


class MP3DownloadPipeline(FilesPipeline):

    def file_downloaded(self, response, request, info):
        print response.headers['Content-Type']
        if response.headers['Content-Type'] != 'audio/mpeg':
            raise DropItem('Item not a valid mpeg file')

        url_parts = list(urlparse.urlparse(request.url))
        query = dict(urlparse.parse_qsl(url_parts[4]))

        path = self.file_path(request, response=response, info=info)
        path = "full/%s.mp3" % query['amifilename123']
        buf = BytesIO(response.body)
        self.store.persist_file(path, buf, info)
        checksum = md5sum(buf)

        return checksum

    def item_completed(self, results, item, info):
        for result in results:
            if result[0] == False:
                return item

        path = os.getcwd() + '/' + settings.get('FILES_STORE')
        tagger = MP3Tagger()
        tagger.do_lookup(
            item,
            "%sfull/%s.mp3" % (path, item['realfilename'])
        )

        return item


class MP3TaggerPipeline(object):
    def process_item(self, item, spider):
        return item


class MP3DownloadPipeline_OLD(MediaPipeline):

    def get_media_requests(self, item, info):
        if item['spidername'] != 'preparer':
            if item['url'] and item['filename']:
                formdata = {
                    'url': item['url'][0],
                    'queue': str(item['queue']),
                    'filename': item['filename'],
                    'realfilename': item['realfilename'],
                }

                request = FormRequest(
                    settings.get('MP3DOWNLOADER_URL'),
                    formdata=formdata,
                    callback=self.check_success,
                    meta={
                        'handle_httpstatus_list': [400],
                        'item': item
                    }
                )

                log.msg("%s ,%s" % (request.url, request.body))

                return request

    def check_success(self, response):
        log.msg("Response from %s:\n%s" % (response.url, response.body))

        if response.status == 200:
            tagger = MP3Tagger()
            tagger.do_lookup(
                response.meta['item'],
                settings.get('MP3DOWNLOADER_SAVEPATH') +
                response.meta['item']['realfilename'] +
                '.mp3'
            )

            return response.meta['item']
