# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field
from scrapy.contrib.loader import ItemLoader
from scrapy.contrib.loader.processor import TakeFirst, MapCompose, Join


class MP3Item(Item):
    spidername = Field()

    realfilename = Field()
    filename = Field()
    files = Field()
    url = Field()
    time = Field()
    size = Field()
    bitrate = Field()

    match = Field()
    queue = Field()

    artist = Field()
    recording = Field()
    release = Field()


class MP3ItemLoader(ItemLoader):
    default_item_class = MP3Item
    default_output_processor = Join()